﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
public class MovePaddle2 : MonoBehaviour {

    Rigidbody rb;
    public float force = 10;
	// Use this for initialization
	void Start ()
    {
        rb = GetComponent<Rigidbody>();
        rb.useGravity = false;
    
	}
	
	// Update is called once per frame
	void FixedUpdate () {

        Vector3 direction;
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        direction = new Vector3(x, 0, z);

        rb.AddForce(direction * force);


    }
}
