﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI : MonoBehaviour {


    public Transform[] points;
    public float speed = 5;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        for (int i = 0; i < points.Length -1; i++)
        {
            transform.Translate(points[i].position * speed * Time.deltaTime);

            if (Vector3.Distance(points[i].position, transform.position)< 2f)
            {
                i++;
            }

            
        }
	}
}
