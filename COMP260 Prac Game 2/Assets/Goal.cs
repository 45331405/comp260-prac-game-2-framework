﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(AudioSource))]
public class Goal : MonoBehaviour {


    public int player;
    public AudioClip scoreClip;
    private AudioSource audio;
    // Use this for initialization
    void Start () {
        audio = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider collider)
    {

        if (collider.tag == "Puck")
        {
            // play score sound
            audio.PlayOneShot(scoreClip);

            scoreKeeper.Instance.OnScoreGoal(player);


            PuckControl puck =
                            collider.gameObject.GetComponent<PuckControl>();
            puck.ResetPosition();
        }
      
    }
}
