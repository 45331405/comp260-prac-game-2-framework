﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class scoreKeeper : MonoBehaviour {

    public int pointsPerGoal = 1;
    private int[] score = new int[2];

    public Text[] scoreText;    public GameObject WB;    public Text wineer_text;
    static private scoreKeeper instance;
    static public scoreKeeper Instance
    {
        get { return instance; }
    }



    // Use this for initialization
    void Start () {
        WB.SetActive(false);
        if (instance == null)
        {
            // save this instance
            instance = this;
        }
        else
        {
            // more than one instance exists
            Debug.LogError(
            "More than one Scorekeeper exists in the scene.");
        }

        for (int i = 0; i < score.Length; i++)
        {
            score[i] = 0;
            scoreText[i].text = "0";
        }

    }
    public void OnScoreGoal(int player)
    {
        score[player] += pointsPerGoal;
        scoreText[player].text = score[player].ToString();
        if (score[player] <= 10)
        {
            endGame( player + 1);
        }
    }

    public void endGame( int player)
    {
        WB.SetActive(true);
        wineer_text.text = "PLAYER" + player.ToString() + "WINS";
        Time.timeScale = 0;
    }


    // Update is called once per frame
    void Update ()
    {
        
	}
}
